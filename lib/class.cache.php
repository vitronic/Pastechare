<?php

/* ===================================
* Author: Nazarkin Roman
* -----------------------------------
* Contacts:
* email - roman@nazarkin.su
* icq - 642971062
* skype - roman444ik
* -----------------------------------
* GitHub:
* https://github.com/NazarkinRoman
* ===================================
*/

class MicroCache {
	public $patch = 'cache/';
	public $lifetime = 31536000; /*mas o menos un año*/
	public $c_type = 'memcache';
	public $cache_on = true;
	public $is_cached = false;
	public $memcache_compressed = false;
	public $file, $key;
	private $memcache;

	function __construct($key=false) {

		class_exists('Memcache') or $this->c_type = 'file';

		if($this->c_type != 'file'){
			$this->memcache = new Memcache;
			@$this->memcache->connect('localhost', 11211) or $this->c_type = 'file';
		}
                /*
                if($key===false){
                   $this->key = md5($_SERVER["REQUEST_URI"]);
                }else{
                   $this->key = md5($key);
                }*/

		$this->key = md5( $key===false ? $_SERVER["REQUEST_URI"] : $key);
		$this->file = $this->patch . $this->key . '.cache';
	}

	public function check() {
		return $this->is_cached = !$this->cache_on ? false
			: $this->c_type == 'file' ?
				( is_readable($this->file) and is_writable($this->file) and (time()-filemtime($this->file) < $this->lifetime) )
				: $this->is_cached = ( $this->cache_on and $this->memcache->get($this->key) );
                
	}

	public function out() {
            return !$this->is_cached ? '' : $this->c_type == 'file' ? file_get_contents($this->file) : $this->memcache->get($this->key);
            /*    
            if (empty($this->is_cached) == false) {
                  if ($this->c_type == 'file') {
                      return file_get_contents($this->file);
                  } else {
                      return $this->memcache->get($this->key);
                  }
            }
            */
	}

	public function start() {
		ob_start();
	}

	public function end() {
		$buffer = ob_get_contents();
		ob_end_clean();

		$this->cache_on and $this->write($buffer);

		unset($buffer);
	}

	public function write($buffer = ''){
		if($this->c_type == 'file') {
			$fp = @fopen($this->file, 'w') or die("No he podido almacenar el archivo : {$this->file}");
			if ( @flock($fp, LOCK_EX)) {
				fwrite($fp, $buffer);
				fflush($fp);
				flock($fp, LOCK_UN);
				fclose($fp);
			}
		} else $this->memcache->set($this->key, $buffer, $this->memcache_compressed, $this->lifetime);
	}

	public function clear($key=false){
		$key = $key === false ? $this->key : md5($key);
		$file = $this->patch . $this->key . '.cache';
		if ($this->c_type == 'file') @unlink($file);
		else $this->memcache->delete($key);
	}

}
