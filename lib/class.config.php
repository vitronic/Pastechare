<?php

/* lib/class.config.php
 *
 * Clase para generar la configuracion de pastechare
 *
 *
 * Copyright (C) 2014 by Díaz Víctor (vitronic)  <vitronic2@gmail.com>
 * Este archivo es parte de Pastechare
 * http://pastechare.linuxd.org/
 *
 * USO
 *      new config('/etc/pastechare/pastechare.ini');
 *
 */

class config {

    private $config; /* propiedad config */
    private $version = '0.9-alpha'; /* version de guachi */

    /* Contructor, inicializa la clase
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    public function __construct($config) {
        $this->config = $config;
        $this->set_config($this->config);
    }

    /* Destructor, destruye automaticamente la clase.
     *
     *
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    public function __destruct() {
        
    }

    /* Genera las definiciones del proyecto tomadas de
     * el parametro de entrada, debe ser un archivo .ini
     *
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    private function set_config($param) {
        $config = parse_ini_file($param, true);
        foreach ($config as $bloque => $conf) {
            foreach ($config[$bloque] as $var => $valor) {
                define(trim($var), trim($valor));
            }
        }
        header("X-Powered-By: Guachi (Lightweight and very simple php framework) v$this->version");
    }

}

?>
