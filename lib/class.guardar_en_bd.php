<?php

/* lib/class.registro.php
 *
 * Clase de registro
 *
 *
 * Copyright (C) 2014 by Díaz Víctor (vitronic)  <vitronic2@gmail.com>
 * Este archivo es parte de Pastechare
 * http://pastechare.linuxd.org/
 *
 * USO
 *
 *   $datos = array(
 *   'usuario' => "vitronic",
 *   'nombre' => 'Máster Vitronic',
 *   'contrasena' => '123'
 *   );
 *   $no_incluir=array('nombre');
 *   $registrar = new registros($cbd);
 *   $registrar->guardar_datos($datos,'usuarios',$no_incluir);
 *
 *   Esto deberia ejecutar lo siguiente
 *   insert into usuarios(usuario,contrasena)values('vitronic','123')
 *   notese que se hizo una excepcion sobre el nombre y no sera parte de la
 *   sentencia
 *
 *  Metodo nueva_conexion
 *      $registrar->nueva_conexion($_SESSION['usuario']);
 */

class guardar_en_bd {

    private $dbdc; /* propiedad data base desde clase :-D */
    public $datos; // datos en array
    public $tabla; /* nombre de la tabla */
    /* Contructor, inicializa la clase
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    public function __construct($dbdc) {
        if (!isset($_SESSION)) {
            session_start();
        }
        $this->dbdc = $dbdc;
    }

    /* Destructor, destruye automaticamente la clase.
     *
     *
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    public function __destruct() {
         unset($this->dbdc);
    }

    public function date_time() {
        return date('Y-m-d h:i:s');
    }

    public function check_datos($condicion, $_datos, $verificar, $tabla = false) {
        $mensajes = new stdClass();
        switch ($condicion) {
            case 'existe':
                if (isset($verificar) and is_array($verificar)) {
                    foreach ($verificar as $campo => $valor) {
                        /* aqui deberia hacer una condicion para saber si la tabla existe */
                        if ($this->dbdc->get_var("select $valor from $tabla where $valor='$_datos[$valor]'")) {
                            $mensajes->mensaje = "ERROR: " . strtoupper($valor) . " $_datos[$valor] existe, imposible proceder";
                            break;
                        }
                    }
                    if (isset($mensajes->mensaje)) {
                        echo json_encode($mensajes);
                        unset($mensajes);                       
                        return true;
                    } else {
                        return false;
                    }
                }
                break;
            case 'es_nulo':
                if (isset($verificar) and is_array($verificar)) {
                    foreach ($verificar as $campo => $valor) {
                        if (empty($_datos[$valor])) {
                            $mensajes->mensaje = "ERROR: $valor no puede ser nulo, imposible proceder";
                            break;
                        }
                    }
                    if (isset($mensajes->mensaje)) {
                        echo json_encode($mensajes);
                        unset($mensajes);
                        return true;
                    } else {
                        return false;
                    }
                }
                break;
        }
    }

    /* Elimina, de un array los campos declarados
     *
     *
     *
     * ENTRADA: - array(asociativo),array(simple)
     * SALIDA:  -
     * ERROR:   -
     */

    public function no_incluir($_datos, $no_incluir) {
        if (isset($no_incluir) and is_array($no_incluir)) {
            foreach ($no_incluir as $campo => $valor) {
                unset($_datos[$valor]);
            }
            return $_datos;
        } else {
            return $_datos;
        }
    }

    /* reemplaza los campos de un array a partir de otro array
     *
     *
     *
     * ENTRADA: - array(asociativo),array(asociativo)
     * SALIDA:  - array() modificado
     * ERROR:   - false
     */

    public function reemplazar($_datos, $_modificar) {
        if (is_array($_datos) and is_array($_modificar)) {
            $datos = array_replace($_datos, $_modificar);
            return $datos;
        } else {
            return false;
        }
    }

    /* Guarda los datos en la db a partir de un array asociativo
     * el array() $no_incluir debe ser un array simple y representa
     * los campos que no seran guardados
     *
     *
     * ENTRADA: - array(asociativo),nombre_tabla,array(simple)
     * SALIDA:  - {ok}
     * ERROR:   - false
     */

    public function guardar_datos($_datos, $_tabla, $salida = false) {
            $sql = "insert into $_tabla";
            $sql .= "(" . implode(",", array_keys($_datos)) . ")";
            $sql .= "values('" . implode("','", $_datos) . "') ";
            //echo "$sql\n";
            if ($this->dbdc->query($sql) == true) {
                if ($salida == 'json') {
                    $mensajes = new stdClass();
                    $mensajes->mensaje = 'ok';
                    echo json_encode($mensajes);
                    unset($mensajes);
                    return true;
                } else {
                    return true;
                }
            } else {
                return false;
            }
    }

    /* retorna la IP del visitante ¿alguna mejora para esto?
     *
     *
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    public function ip() {
        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $_SERVER["REMOTE_ADDR"] = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        $ip = $_SERVER['REMOTE_ADDR'];
        return $ip;
    }

    /* registra una nueva conexion en la base de datos
     *
     *
     *
     * ENTRADA: - $_SESSION['usuario']
     * SALIDA:  -
     * ERROR:   -
     */

    public function nueva_conexion($usuario) {
        $id_usuario = $this->dbdc->get_var("select id from usuarios where usuario='$usuario'");
        $this->token_sesion = $_SESSION['token_sesion'];
        if (!$this->dbdc->get_var("select id from conexiones where token_sesion='$this->token_sesion'")) {
            $datos = array(
                'id_usuario' => $id_usuario,
                'token_sesion' => $this->token_sesion,
                'ip_sesion' => $this->ip(),
                'fecha_registro' => $this->date_time(),
                'estatus' => 1
            );
            $this->guardar_datos($datos, 'conexiones', false);
        }
    }

    /* retorna el id de una conexion
     *
     * ENTRADA: - $_SESSION['token_sesion']
     * SALIDA:  - id_conexion
     * ERROR:   - false
     */

    public function id_conexion($token_sesion) {
        $id_conexion = $this->dbdc->get_var("select id from conexiones where token_sesion='$token_sesion'");
        if ($id_conexion) {
            return $id_conexion;
        } else {
            return false;
        }
    }
    
    /* retorna un CRC (Cyclic redundancy check) aleatorio para usar
     *
     * ENTRADA: - 
     * SALIDA:  - 
     * ERROR:   - string
     */    
    public function crc_url() {
        return hash('crc32b', md5(uniqid(rand(), 1)));
    }

}

?>
