<?php
/* lib/class.pantilla.php
 *
 * pequeña clase para el manejo de plantillas
 *
 *
 * Copyright (C) 2014 by Díaz Víctor (vitronic)  <vitronic2@gmail.com>
 * basado en http://www.cleverlogic.net/tutorials/building-simple-php-templating-class
 *
 */

class plantilla {

    private $plantilla;

    /* Contructor, inicializa la clase
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    function __construct($plantilla = null) {
        if (isset($plantilla)) {
            $this->carga($plantilla);
        }
    }

    /* Destructor, destruye automaticamente la clase.
     *
     *
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    public function __destruct() {
        unset($this->plantilla);
    }

    /* Elimina los saltos de linea asi como los espacios de
     * la entrada del buffer
     *
     *
     * ENTRADA: - string
     * SALIDA:  - string comprimido
     * ERROR:   -
     */
    static function comprimir($buffer) {
        $busca = array('/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s');
        $reemplaza = array('>', '<', '\\1');
        return preg_replace($busca, $reemplaza, $buffer);
        unset($buffer);
    }

    /* Carga la plantilla
     *
     *
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    public function carga($plantilla) {
        if (is_file($plantilla)) {
            $this->plantilla = $plantilla;
        }
    }

    /* establece las variables que seran pasadas a la plantilla
     *
     *
     *
     * ENTRADA: - variable , valor
     * SALIDA:  -
     * ERROR:   -
     */

    public function establece($variable, $contenido) {
        $this->$variable = $contenido;
    }

    /* Imprime en pantalla la plantilla que se a cargado
     *
     *
     *
     * ENTRADA: - string
     * SALIDA:  - string
     * ERROR:   -
     */

    public function muestra($modo = false) {
        ob_start();
        require $this->plantilla;
        $contenido = ob_get_clean();
        switch ($modo) {
            case 'comprimido':
            echo $this->comprimir($contenido);                
                break;
            default:
            echo $contenido;
                break;
        }
    }

    /* Inserta una nueva plantilla si ya se ha cargado una previamente
     *
     *
     *
     * ENTRADA: - string
     * SALIDA:  -
     * ERROR:   -
     */

    public function inserta() {
        ob_start();
        require $this->plantilla;
        $contenido = ob_get_clean();
        return $contenido;
    }

}

?>
