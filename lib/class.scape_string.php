<?php

/* lib/class.scape_string.php
 *
 * Clase para escapar las cadenas
 *
 *
 * Copyright (C) 2014 by Díaz Víctor (vitronic)  <vitronic2@gmail.com>
 * Este archivo es parte de Pastechare
 * http://pastechare.linuxd.org/
 *
 *  USO
 *      $data = array('usuario' => 'vitronic', 'contrasena' => '123');
 *      $limpiador = new limpiador($data, 'sqlite');
 *      echo $limpiador->campo->usuario;
 *      echo $limpiador->campo->contrasena;
 */

class limpiador {

    public $campo = array();
    public $datos;
    public $tipo;
    public $modo;

    /* Contructor, inicializa la clase
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    public function __construct($datos, $tipo = tipo_db, $modo = 0) {
        if ($modo == 1) {
            $this->recolector($datos, $tipo, 1);
        } else {
            $this->recolector($datos, $tipo);
        }
    }

    /* Destructor
     *
     * ENTRADA: -
     * SALIDA:  -
     * ERROR:   -
     */

    public function __destruct() {
        unset($this->campo);
    }

    /* Un recolector y escapador de datos.
     *
     * ENTRADA : array() en bruto,tipo de db, modo
     *      tipos
     *          limpio = limpia de manera generica el array
     *          pgsql = escapa el array para PostgreSQL
     *          en_bruto = solo convierte el array en objetos
     *          sqlite = escapa el array para SQlite
     *      modos
     *          1 retorna la salida convertida en objetos
     *          false solo retorna, la salida escapada
     *
     * SALIDA : array() escapado 
     * ERROR : - 
     */

    private function recolector($_datos, $tipo, $modo = false) {

        switch ($tipo) {
            case 'limpio':
                foreach ($_datos as $campo => $valor) {
                    $limpiado = trim(htmlentities(addslashes($valor)));
                    $data[$campo] = $limpiado;
                }
                break;
            case 'pgsql':
                foreach ($_datos as $campo => $valor) {
                    $limpiado = trim(htmlentities(pg_escape_string($valor)));
                    $data[$campo] = $limpiado;
                }
                break;
            case 'en_bruto':
                foreach ($_datos as $campo => $valor) {
                    $data[$campo] = $valor;
                }
                break;
            case 'sqlite':
                foreach ($_datos as $campo => $valor) {
                    $limpiado = trim(htmlentities(SQLite3::escapeString($valor)));
                    $data[$campo] = $limpiado;
                }
                break;
        }
        if (isset($data) and $modo == 1) {
            $this->campo = (object) $data;
        } elseif (isset($data) and !$modo) {
            $this->campo = $data;
        }
    }

}

?>
