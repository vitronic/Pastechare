function comentarios() {
    $.ajax({
        type: "GET",
        async: true,
        // cache: true,
        dataType: 'json',
        url: "/mostrar_comentarios",
        beforeSend: function() {
            $('#lista_comentarios').html('Cargando comentarios , espera ...');
        },
        success: function(respuesta) {
            if (respuesta.mensaje === 'ok') {
                $("#lista_comentarios").html(respuesta.comentarios);
            }else{
                $("#lista_comentarios").html(respuesta.no);                
            }

        }
    });
};

$("#nuevo_comentario").click(function() {
    var formulario = $("#form_comentar").serializeArray();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: "/post",
        data: formulario,
        beforeSend: function() {
            $(".mensaje").css("display", "block");
            $('#mensaje').html('Procesando tu comentario, espera ...');
        },
        success: function(respuesta) {
            if (respuesta.mensaje === 'ok') {
                $(".mensaje").css("display", "none");
                document.getElementById("comentario").value = ""
                comentarios();
            } else {
                $(".mensaje").css("display", "block");
                $("#mensaje").html(respuesta.mensaje);
            }

        }
    });

});

comentarios();
