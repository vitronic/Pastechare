/*rutinas javascript's para esta pagina*/
$(document).ready(function() {
    $(".chosen-select").chosen({no_results_text: "Oops, nada encontrado!"});
    function recapthca() {
        $.ajax({
            type: "GET",
            async: true,
            cache: true,
            url: "/captcha",
            beforeSend: function() {
                $('#img_captcha').attr('src', '/img/carga_captcha.gif')
            },
            success: function() {
                $('#img_captcha').attr('src', '/captcha&' + (new Date()).getTime());
            }
        });
    };
    $("#recaptcha").click(function() {
        recapthca();
    });
});
$("#form_pastechare").on("valid", function(form) {
    form.stopPropagation();
    form.preventDefault();
    if (form.type === "valid") {
        var formulario = $("#form_pastechare").serializeArray();
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/post",
            data: formulario,
            beforeSend: function() {
                $(".mensaje_index").css("display", "block");
                $('#mensaje_index').html('Procesando su pastechare, espere ...');
            },
            success: function(respuesta) {
                if (respuesta.mensaje === 'ok') {
                    if (respuesta.paste) {
                        window.location = respuesta.paste;
                    }
                } else {
                    $(".mensaje_index").css("display", "block");
                    $("#mensaje_index").html(respuesta.mensaje);
                }

            }
        });
    }
});