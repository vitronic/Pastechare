/*rutinas javascript's para esta pagina*/
$(document).ready(function() {
    function reset_contrasena() {
        document.getElementById("contrasena").value = ""
    }
    $("#form_entrada").on("valid", function(form) {
        form.stopPropagation();
        form.preventDefault();
        if (form.type === "valid") {
            usuario = document.getElementById("usuario").value;
            contrasena = document.getElementById("contrasena");
            if (contrasena.value !== "") {
                contrasena.value = Whirlpool(contrasena.value + usuario);
            } else {
                reset_contrasena();
            }
            var formulario = $("#form_entrada").serializeArray();
            $.ajax({
               // async: true,
               // cache: false,
                type: "POST",
                dataType: 'json',
                url: "/post",
                data: formulario,
                beforeSend: function() {
                    $(".mensaje").css("display", "block");
                    $('#mensaje').html('Procesando tu autenticación, espera ...');
                },
                success: function(respuesta) {
                    if (respuesta.mensaje === 'ok') {
                        location.reload();
                    } else {                    
                        $(".mensaje").css("display", "block");
                        $("#mensaje").html(respuesta.mensaje);
                        reset_contrasena();
                    }

                }
            });
        }
    });

});




