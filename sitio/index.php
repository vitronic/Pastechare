<?php

/*
   ____           _            _
  |  _ \ __ _ ___| |_ ___  ___| |__   __ _ _ __ ___
  | |_) / _` / __| __/ _ \/ __| '_ \ / _` | '__/ _ \
  |  __/ (_| \__ \ ||  __/ (__| | | | (_| | | |  __/
  |_|   \__,_|___/\__\___|\___|_| |_|\__,_|_|  \___|

  Copyright © 2014 Díaz Víctor aka (Máster Vitronic)
 */

chdir('..');
require 'config.php';
require_once 'lib/pastechare.php';
require_once 'lib/class.guardar_en_bd.php';
require_once 'lib/class.auth.php';
require_once 'lib/class.scape_string.php';
require_once 'lib/class.captcha.php';
require_once 'lib/class.plantilla.php';
require_once 'lib/class.geshi.php';
$registrar = new guardar_en_bd($cbd);
$autenticar = new auth($cbd);
$autenticar->guardian_sesion();
$autenticar->sesion_anonima();
$registrar->nueva_conexion($_SESSION['usuario']);
if ($_GET) {
    foreach ($_GET as $campo => $valor) {
        $campo = str_replace('/', '', $campo);   /* si tiene un / al inicio lo remuevo */
        if (stristr($campo, '=', true)) {
            $campo = stristr($campo, '=', true); /* remuevo cualquier valor. (campo=valor retorna campo) */
        }
        if (isset($campo)) {
            if (is_file('modulos/' . $campo . '.php') == true) {
                require_once 'modulos/' . $campo . '.php';
                break;
            } else {
                require_once "modulos/error.php";
                break;
            }
        }
    }
} else {
    require_once "modulos/inicio.php";
}
?>
