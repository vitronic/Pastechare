<?php
/*
   ____           _            _
  |  _ \ __ _ ___| |_ ___  ___| |__   __ _ _ __ ___
  | |_) / _` / __| __/ _ \/ __| '_ \ / _` | '__/ _ \
  |  __/ (_| \__ \ ||  __/ (__| | | | (_| | | |  __/
  |_|   \__,_|___/\__\___|\___|_| |_|\__,_|_|  \___|

  Copyright © 2014 Díaz Víctor aka (Máster Vitronic)
 */
    header("HTTP/1.0 404 Not Found");
    $pagina = new plantilla('vistas/principal.html.php');
  //  $menu_superior = new plantilla();
  //  $menu_superior->carga('vistas/menu_superior.html.php');
    $cuerpo = new plantilla();
    $cuerpo->time_start = microtime();
    $cuerpo->carga('vistas/error.html.php');
    $pagina->tittulo = 'Pagina no encontrada';
    $pagina->descripcion = 'Pastechare es un sitio para publicar y/o corregir codigo fuente, el codigo es coloreado para facilitar su lectura';
    $pagina->keywords = 'pastebin,compartir codigo,pegar codigo,colorear codigo,codigo fuente';
    $css = array(
    'normalize.min.css',
    'foundation.min.css',
    'foundation-icons.min.css',
    'comun.css',
    'error.html.css'
    );
    $pagina->css = $css;
   // $pagina->establece('menu_superior', $menu_superior->inserta());
    $pagina->establece('cuerpo', $cuerpo->inserta());
    $pagina->muestra();
    unset($pagina,$cuerpo);
?>
