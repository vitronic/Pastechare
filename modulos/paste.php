<?php

/*
  ____           _            _
  |  _ \ __ _ ___| |_ ___  ___| |__   __ _ _ __ ___
  | |_) / _` / __| __/ _ \/ __| '_ \ / _` | '__/ _ \
  |  __/ (_| \__ \ ||  __/ (__| | | | (_| | | |  __/
  |_|   \__,_|___/\__\___|\___|_| |_|\__,_|_|  \___|

  Copyright © 2014 Díaz Víctor aka (Máster Vitronic)
 */
require_once('lib/class.cache.php');
$pagina = new plantilla('vistas/principal.html.php');
$cuerpo = new plantilla();
$cuerpo->time_start = microtime();
$opciones = new plantilla();
$cuerpo->carga('vistas/paste.html.php');
$pagina->descripcion = 'Pastechare es un sitio para publicar y/o corregir codigo fuente, el codigo es coloreado para facilitar su lectura';
$pagina->keywords = 'pastebin,compartir codigo,pegar codigo,colorear codigo,codigo fuente';
if ($_SESSION['usuario'] !== 'anonimo') {
    $cuerpo->opciones = $opciones->inserta($opciones->carga('vistas/paste_opciones.html.php'));
}
$css = array(
    'normalize.min.css',
    'foundation.min.css',
    'foundation-icons.min.css',
    'comun.css',
    'paste.html.css'
);
$pagina->css = $css;
$limpiar = new limpiador($_GET, tipo_db, true);
if (isset($limpiar->campo->paste)) {
    $url = $limpiar->campo->paste;
    $paste = $cbd->get_row("select url,codigo,lenguaje,titulo from codigos where url='$url'");
}
if (isset($paste->url)) {
    $pagina->tittulo = "$paste->titulo -- Pastechare";
    $geshi = new GeSHi(html_entity_decode(htmlspecialchars_decode($paste->codigo)), $paste->lenguaje);
    $geshi->set_encoding('utf-8');
    $geshi->enable_line_numbers(GESHI_FANCY_LINE_NUMBERS);
    $geshi->set_case_keywords(GESHI_CAPS_LOWER);
    $geshi->set_header_type(GESHI_HEADER_DIV);
    //$geshi->set_footer_content('Generado en <TIME> segundos');
    $geshi->set_link_target('_blank');
    $cache = new MicroCache($paste->url);
    if ($cache->check()) {
        $cuerpo->establece('codigo', $cache->out());
        $pagina->establece('cuerpo', $cuerpo->inserta());
        $pagina->muestra();
    } else {
        $cache->start();
        echo '<!--Servido desde la cache con fecha: '.strftime("%A %d de %B del %Y").' '.date('h:i:s A') . "-->\n";
        echo plantilla::comprimir($geshi->parse_code()) . "\n";
        echo "<!--Fin de bloque en cache-->\n";
        $cache->end();
        $cuerpo->establece('codigo', $geshi->parse_code());
        $pagina->establece('cuerpo', $cuerpo->inserta());
        $pagina->muestra();
    }
} else {
    require_once "modulos/error.php";
}
unset($pagina, $cuerpo, $opciones);
die();
?>
