<?php
/*
   ____           _            _
  |  _ \ __ _ ___| |_ ___  ___| |__   __ _ _ __ ___
  | |_) / _` / __| __/ _ \/ __| '_ \ / _` | '__/ _ \
  |  __/ (_| \__ \ ||  __/ (__| | | | (_| | | |  __/
  |_|   \__,_|___/\__\___|\___|_| |_|\__,_|_|  \___|

  Copyright © 2014 Díaz Víctor aka (Máster Vitronic)
 */

paranoia();
@$paste = str_ireplace("=", "", stristr($_SERVER['HTTP_REFERER'], '=', false));
if($paste){
$mensajes = new stdClass();
$cbd->use_disk_cache = false;
$comentarios = $cbd->get_results("select usuarios.usuario,comentarios.comentario
                                from usuarios,comentarios where usuarios.id=comentarios.id_usuario
                                and comentarios.id_codigo = (select id from codigos
                                where url='".$paste."') order by comentarios.id desc");
if(isset($comentarios)){
foreach ($comentarios as $comentario) {
$mensajes->comentarios .= '
<div class="row">
    <div class="large-2 columns small-3 img_icon_usuario"><img src="/img/anonimo_80x80.png"/></div>
    <div class="large-10 columns">
        <p><strong>'.$comentario->usuario.' comenta:</strong> '.$comentario->comentario.'.</p>
        <ul class="inline-list">
            <li><a class="replicar" href="#">Replicar</a></li>
            <li><a class="compartir" href="#">Compartir</a></li>
        </ul>
    </div>
</div>';
}
$mensajes->mensaje = 'ok';
echo json_encode($mensajes);
}else{
$mensajes->no = '
<br/><br/>
<br/>
<div data-alert class="alert-box info radius">
No hay comentarios en este pastechare.
<a href="#" class="close">&times;</a>
</div>';
echo json_encode($mensajes); 
}
unset($mensajes);
}
?>
