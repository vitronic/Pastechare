<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <?php echo "<title>". $this->tittulo . "</title>" . "\n";?>
        <meta name="author" content="Díaz Víctor (vitronic)">
        <meta name="description" content="<?php echo $this->descripcion;?>">
        <meta name="keywords" content="<?php echo $this->keywords;?>">
        <meta name="application-name" content="Pastechare">
        <meta name="generator" content="Guachi - (Lightweight and very simple php framework) - Version 0.1-alpha">
<?php if (isset($this->css)):?>
<?php foreach ( $this->css as $css) :?>
        <link type="text/css" rel="stylesheet" href="/css/<?php echo $css;?>">
<?php endforeach;?>
<?php endif;?>
        <script async src="/js/vendor/modernizr.js"></script>
