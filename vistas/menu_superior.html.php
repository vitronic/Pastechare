    <div class="row">
        <div class="large-12 columns">
            <nav class="top-bar" data-topbar>
                <ul class="title-area">
                    <li class="name">
                        <h1><a href="/">Pastechare</a></h1>
                    </li>
                    <li class="toggle-topbar menu-icon">
                        <a href="#"><span>menu</span></a>
                    </li>
                </ul>
                <section class="top-bar-section">
<?php if ($_SESSION['usuario'] !== 'anonimo'): ?>
                        <ul class="left">
                            <li>
                                <a class="text-center " href="/usuario/<?php echo $_SESSION['usuario']; ?>">Bienvenid@ <?php echo $_SESSION['usuario']; ?></a>
                            </li>
                            <li class="has-button">
                                <a href="/salir" class="small has-button text-center">Salir</a>
                            </li>
                        </ul>

<?php else: ?>
                        <ul class="left" >
                            <li class="has-button">
                                <a href="/entrada" class="small has-button text-center" data-reveal-ajax="true" data-reveal-id="entrada">Entra</a>
                            </li>
                            <li class="has-button">
                                <a href="/registro" class="small has-button text-center"  data-reveal-ajax="true" data-reveal-id="registrate">Registrate</a>
                            </li>
                        </ul>
<?php endif; ?>
                        <ul class="right">
                                <li class="search">
                                    <form id="form_buscar">
                                    <input  id="buscador" class="text-center" type="search">
                                    </form>
                                </li>
                                <li class="has-button">
                                    <a class="small button " id="buscar">Buscar</a>
                                </li>
                        </ul>
                    <div id="entrada" class="reveal-modal tiny" data-reveal>
                    </div>
                    <div id="registrate" class="reveal-modal medium" data-reveal>
                    </div>
                </section>
            </nav>
        </div>
    </div>
    <div class="row">
        <br/>
    </div>
